#pragma once

#include "config.h"
#include "util.h"
#include "grid.h"
#include "model.h"
#include "world.h"

#include <driver_types.h>
#include <Eigen/Dense>
#include <glad/gl.h>
#include <nvfunctional>

struct SimParams {
	float timeStep;
	Eigen::Vector3f gravity;
};

struct Simulator {

	template <class F>
	void foreachPoint(F f) {
		gpu::foreach<Particle>(_world.devicePtr(), _world.numParticles(), f);
	}

	template <class F>
	__device__
	// re: gridWidth & cellSize; dumb cuda thing, gotta pass this in to get my epic nested lambdas to work :(
	void interpolate(u32 gridWidth, float cellSize, const Eigen::Vector3f &pos, F f) {
		auto nPos = (pos / cellSize);
		auto nearest = (nPos + Eigen::Vector3f{0.5f, 0.5f, 0.5f}).template cast<int>();
		for (int dx = -2; dx <= 2; dx++) {
			for (int dy = -2; dy <= 2; dy++) {
				for (int dz = -2; dz <= 2; dz++) {
					auto newPos = nearest + Eigen::Vector3i(dx, dy, dz);
					if (newPos[0] < 0 || newPos[0] >= gridWidth
					|| newPos[1] < 0 || newPos[1] >= gridWidth
					|| newPos[2] < 0 || newPos[2] >= gridWidth)
						continue;
					f(newPos, nPos - newPos.template cast<float>());
				}
			}
		}
	}

	Simulator(const Configuration &cfg, World &world, Grid &grid, SimParams params, GLuint vbo);

	void simulate();

	void particleToGrid();
	void stepGrid();
	void gridToParticle();

	void drawToGL();

private:
	const Configuration &cfg;
	World &_world;
	Grid &_grid;
	SimParams _params;

	cudaGraphicsResource_t out_vbo = nullptr;
};