#include "simulator.h"
#include "debug.h"

#include "cuda_gl_interop.h"

#include <svd/svd3_cuda.h>

#include <iostream>
#include <cstdio>
#include <random>
#include <utility>

__device__
static float N(float x) {
	x = std::abs(x);
	if (x < 0.5f) {
		return 0.75f - x * x;
	} else if (x < 1.5f) {
		return 0.5f * (1.5f - x) * (1.5f - x);
	} else {
		return 0;
	}
}

__device__
static float weight(const Eigen::Vector3f &diff) {
	return N(diff[0]) * N(diff[1]) * N(diff[2]);
}

__device__
static int pos2grid(const Eigen::Vector3i &pos, const int &gridWidth) {
	return pos[2] * (gridWidth * gridWidth) + pos[1] * gridWidth + pos[0];
}

Simulator::Simulator(const Configuration &cfg, World &world, Grid &grid, SimParams params, GLuint vbo)
	: cfg(cfg), _world(world), _grid(grid), _params(std::move(params)) {
	CHECK_CUDA_ERROR(cudaGraphicsGLRegisterBuffer(&out_vbo, vbo, cudaGraphicsMapFlagsWriteDiscard));
}

// Advance the simulation by one tick
void Simulator::simulate() {
	// (1) Nuke the grid
	CHECK_KERNEL_ERROR(_grid.reset());

	// (2) Transfer to grid
	CHECK_KERNEL_ERROR(particleToGrid());

	// (3) Apply forces on grid
	CHECK_KERNEL_ERROR(stepGrid());

	// (4) Transfer back to points
	CHECK_KERNEL_ERROR(gridToParticle());

	// (5) Update OpenGL's particle locations
	drawToGL();
}

void Simulator::drawToGL() {
	// (5) Redraw screen with new point data
	CHECK_CUDA_ERROR(cudaGraphicsMapResources(1, &out_vbo, nullptr));

	float4 *data;
	u64 size;
	CHECK_CUDA_ERROR(cudaGraphicsResourceGetMappedPointer((void **)&data, &size, out_vbo));
	assert(data != nullptr);
	assert(size >= _world.numParticles() * sizeof(float4));

	foreachPoint([=] __device__ (u32 i, Particle &p) {
		data[i] = make_float4(
				p.pos[0], p.pos[1], p.pos[2], 1.0f);
	});

	CHECK_CUDA_ERROR(cudaGraphicsUnmapResources(1, &out_vbo, nullptr));
}

void Simulator::particleToGrid() {
	// Transfer mass, velocity, and force to the grid
	auto cellSize = _grid.cellSize();
	auto gridWidth = _grid.width();
	auto grid = _grid.devicePtr();
	auto dt = _params.timeStep;

	// First pass: contribute mass!
	foreachPoint([=] __device__ (u32 i, Particle &p) {
		interpolate(gridWidth, cellSize, p.pos, [=] __device__ (auto &newPos, auto diff) {
			int idx = pos2grid(newPos, gridWidth);
			atomicAdd(&grid[idx].mass, p.mass * weight(diff));
		});
	});

	// Second try: make 'em fly!
	foreachPoint([=] __device__ (u32 i, Particle &p) {
		// Re-calculate volume
		auto density = 0.0f;
		interpolate(gridWidth, cellSize, p.pos, [=, &density] __device__ (auto &newPos, auto diff) {
			int idx = pos2grid(newPos, gridWidth);
			density += grid[idx].mass * weight(diff);
		});
		p.volume = p.mass / density * (cellSize * cellSize * cellSize);

		// Next, compute and push momentum
		auto cs = -dt / (cellSize * cellSize) * p.stress();
		auto affine = cs + p.mass * p.C; // Fused APIC momentum + MLS-MPM stress
		interpolate(gridWidth, cellSize, p.pos, [=] __device__ (auto &newPos, auto diff) {
			float w = weight(diff);
			auto dx = diff * cellSize;
			auto mv = p.vel * p.mass;
			auto dv = (mv + affine * dx) * w;
			int idx = pos2grid(newPos, gridWidth);
			atomicAdd(&grid[idx].vel[0], dv[0]);
			atomicAdd(&grid[idx].vel[1], dv[1]);
			atomicAdd(&grid[idx].vel[2], dv[2]);
		});
	});
}

void Simulator::stepGrid() {
	// First, apply forces and update grid velocities
	auto gravity = _params.gravity;
	auto dt = _params.timeStep;
	auto gridWidth = _grid.width();
	gpu::foreach(_grid.devicePtr(), _grid.size(), [=] __device__ (u32 i, Cell &c) {
		if (c.mass > 1e-8f) { // Ignore empty cells
			float invMass = 1.0f / c.mass;
			c.vel = c.vel * invMass + dt * gravity;

			// Slip bounds
			for (int j = 0; j < 3; j++) {
				if (c.pos[j] < 2 || c.pos[j] >= (float)gridWidth - 2)
					c.vel[j] = 0;
			}
		}
	});
}

void Simulator::gridToParticle() {
	auto cellSize = _grid.cellSize();
	auto gridWidth = _grid.width();
	auto dt = _params.timeStep;
	auto grid = _grid.devicePtr();
	foreachPoint([=] __device__ (u32 i, Particle &p) {
		p.vel.setZero();
		p.C.setZero();
		interpolate(gridWidth, cellSize, p.pos, [=, &p] __device__ (auto &newPos, auto diff) {
			float w = weight(diff);
			auto dx = diff * cellSize;
			int idx = pos2grid(newPos, gridWidth);
			auto gridV = grid[idx].vel * w;
			p.C += 4.0f / cellSize * gridV * dx.transpose();
			p.vel += gridV;
		});

		p.pos += dt * p.vel;

		// Clamping and bounding
		p.pos.cwiseMax(cellSize);
		p.pos.cwiseMin((gridWidth - 1.0f) * cellSize);
		for (int j = 0; j < 3; j++) {
			if (p.pos[j] < 3 * cellSize)
				p.vel[j] += 3 * cellSize - p.pos[j];
			if (p.pos[j] >= (gridWidth - 3.0f) * cellSize)
				p.vel[j] += (gridWidth - 3.0f) * cellSize - p.pos[j];
		}

		p.F = (Eigen::Matrix3f::Identity() + dt * p.C) * p.F;
	});
}