#include "config.h"
#include "model.h"
#include "renderer.h"
#include "simulator.h"
#include "world.h"

#define GLFW_INCLUDE_NONE

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <random>

static void error_callback(int, const char *);

static void key_callback(GLFWwindow *, int, int, int, int);

static std::vector<Eigen::Vector3f> generateSphere(float radius, float particles = 15000) {
	std::vector<Eigen::Vector3f> vec;
	std::uniform_real_distribution<float> unif(-radius, radius);
	std::default_random_engine re;

	while (vec.size() < particles) {
		Eigen::Vector3f tmp(unif(re), unif(re), unif(re));
		if (tmp.squaredNorm() < radius * radius)
			vec.emplace_back(tmp);
	}
	return vec;
}

int main() {
	// TODO accept program arguments
	Configuration cfg;

	if (!glfwInit()) {
		std::cout << "GLFW initialization failed!" << std::endl;
		return 1;
	}
	glfwSetErrorCallback(error_callback);

	// Create window w/ GL context
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	auto window = glfwCreateWindow(1280, 720, "MyMPM", nullptr, nullptr);
	if (!window) {
		std::cout << "GLFW window creation failed!" << std::endl;
		return 1;
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGL(glfwGetProcAddress);
	glfwSwapInterval(1);

	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	glViewport(0, 0, width, height);

	World world;
	world.addModel(generateSphere(2), {2.5f, 2.5f, 5.5f}, {10.0f, 5.0f, 0.0f});
	world.addModel(generateSphere(2), {7.5f, 2.5f, 4.5f}, {-10.0f, 5.0f, 0.0f});

	Grid grid(cfg.cellSize, cfg.gridLength);
	Renderer renderer(cfg, width, height, world);
	SimParams params = {cfg.timeStep, {0.0f, cfg.gravity, 0.0f}};
	Simulator simulator(cfg, world, grid, params, renderer.getPointVBO());

	while (!glfwWindowShouldClose(window)) {
		simulator.simulate();
		renderer.render();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	return 0;
}

static void error_callback(int error, const char *description) {
	std::cout << "GLFW error: " << description << " (" << error << ")" << std::endl;
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}


