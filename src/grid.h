#pragma once

#include "defs.h"

#include <Eigen/Dense>

struct Cell {
	Eigen::Vector3f pos, vel;
	float mass;
};

struct Grid {
	explicit Grid(float cellSize, u32 width);

	void reset() const;

	Cell *devicePtr() const;

	float cellSize() const;
	u32 width() const;
	u32 size() const;

private:
	Cell *_devicePtr = nullptr;
	float _cellSize;
	u32 _width;
};