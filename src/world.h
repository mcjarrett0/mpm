#pragma once

#include "model.h"

#include <Eigen/Dense>

#include <utility>
#include <vector>

struct Material {
	float lambda, mu, hardening;

	constexpr Material(float young, float poisson, float hardening)
			: lambda((young * poisson) / ((1.0f + poisson) * (1.0f - 2.0f * poisson))),
			  mu(young / (2.0f * (1.0f + poisson))),
			  hardening(hardening) {}
};

constexpr Material DefaultMaterial(1e5f, 0.2f, 10);

struct Particle {
	// Physical state
	Eigen::Vector3f pos;
	Eigen::Vector3f vel;
	Eigen::Matrix3f F = Eigen::Matrix3f::Identity(); // Deformation gradient
	Eigen::Matrix3f C = Eigen::Matrix3f::Zero(); // Affine momentum
	float mass = 1, volume = 1;

	explicit Particle(Eigen::Vector3f pos, Eigen::Vector3f vel, const Material &mat)
			: pos(std::move(pos)), vel(std::move(vel)), mat(mat) {}

	__device__
	Eigen::Matrix3f stress() const;

private:
	const Material mat;
};

struct World {
	void addModel(const Model &model,
				  const Eigen::Vector3f &pos = {0.0f, 0.0f, 0.0f},
				  const Eigen::Vector3f &vel = {0.0f, 0.0f, 0.0f},
				  const Material &mat = DefaultMaterial);

	Particle *devicePtr() const;

	u32 numParticles() const;

private:
	std::vector<Particle> particles;
	Particle *_devicePtr = nullptr;
};