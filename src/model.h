#pragma once

#include "defs.h"

#include <Eigen/Dense>

#include <cstdlib>
#include <fstream>
#include <vector>

// Simple class for storing a set of vertices
class Model {
	std::vector<Eigen::Vector3f> vertices;

public:
	// Load from file
	Model(const std::string &path);

	// Build programmatically
	Model(std::vector<Eigen::Vector3f> vertices);

	const std::vector<Eigen::Vector3f> &getVertices() const {
		return vertices;
	}
};