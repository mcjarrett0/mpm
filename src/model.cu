#include "model.h"

#include <iostream>
#include <utility>

Model::Model(const std::string &path) {
	std::ifstream in(path);
	if (!in.good()) {
		std::cerr << "Error reading obj file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::string line;
	while (std::getline(in, line)) {
		std::stringstream ss(line);
		std::string v;
		ss >> v;
		if (v != "v")
			continue;

		float x, y, z;
		ss >> x >> y >> z;
		vertices.emplace_back(Eigen::Vector3f(x, y, z));
	}

	std::cout << "Loaded model " << path << " with " << vertices.size() << " vertices!" << std::endl;
}

Model::Model(std::vector<Eigen::Vector3f> vertices) : vertices(std::move(vertices)) {
}