#include "renderer.h"
#include "util.h"

#include <glad/gl.h>
#include <glm/ext.hpp>

#include <cmath>
#include <iostream>

static const GLfloat floor_vertices[20] = {
		2.0f, 2.0f, 0.0f, 1.0f, 1.0f,
		0.0f, 2.0f, 0.0f, 0.0f, 1.0f,
		0.0f, 0.0f, 0.0f, 0.0f, 0.0f,
		2.0f, 0.0f, 0.0f, 1.0f, 0.0f,
};

static const GLuint indices[6] = {
		0, 1, 2,
		2, 3, 0,
};

Renderer::Renderer(const Configuration &cfg, u32 width, u32 height, const World &world)
	: proj(glm::perspective(3.14159f * 0.25f, (float)width / height, 0.1f, 1000.0f)),
	  textures(),
	  floor_shader(glu::loadShaders("res/floor_vert.glsl", "res/floor_frag.glsl")),
	  point_shader(glu::loadShaders("res/point_vert.glsl", "res/point_frag.glsl")),
	  world(world) {

	// Floor
	glUseProgram(floor_shader);
	glUniformMatrix4fv(glGetUniformLocation(floor_shader, "proj"), 1, GL_FALSE, glm::value_ptr(proj));

	glGenVertexArrays(1, &floor_vao);
	glBindVertexArray(floor_vao);

	glGenBuffers(1, &floor_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, floor_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(floor_vertices), floor_vertices, GL_STATIC_DRAW);

	// Position
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	// Tex-coord
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	glGenBuffers(1, &floor_ebo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, floor_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// Points
	glm::mat4 model(1.0f);
	glUseProgram(point_shader);
	glUniformMatrix4fv(glGetUniformLocation(point_shader, "proj"), 1, GL_FALSE, glm::value_ptr(proj));
	glUniformMatrix4fv(glGetUniformLocation(point_shader, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glUniform1f(glGetUniformLocation(point_shader, "size"), cfg.pointSize);

	glGenVertexArrays(1, &point_vao);
	glBindVertexArray(point_vao);

	glGenBuffers(1, &point_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, point_vbo);
	glBufferData(GL_ARRAY_BUFFER, world.numParticles() * 4 * sizeof(float), nullptr, GL_DYNAMIC_DRAW);

	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr);
	glEnableVertexAttribArray(0);

	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glEnable(GL_DEPTH_TEST);
}

void Renderer::render() {
	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	static auto time = 0;
	time++;

	auto angle = time / 1550.0f;
	view = glm::lookAt(
			glm::vec3( 5.0f + 10.0f * std::sin(angle), 6.2f, 5.0f + 10.0f * std::cos(angle)),
			glm::vec3(5.0f, 3.0f, 5.0f), glm::vec3(0.0f, 1.0f, 0.0f));

	drawFloor();
	drawPoints();
}

void Renderer::drawFloor() {
	glUseProgram(floor_shader);
	glUniform1i(glGetUniformLocation(floor_shader, "texture1"), 0);
	glBindVertexArray(floor_vao);

	glUniformMatrix4fv(glGetUniformLocation(floor_shader, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glm::mat4 model(1.0f);
	model = glm::scale(model, glm::vec3(5.0f, 5.0f, 5.0f));
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	glUniformMatrix4fv(glGetUniformLocation(floor_shader, "model"), 1, GL_FALSE, glm::value_ptr(model));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

void Renderer::drawPoints() {
	glUseProgram(point_shader);
	glBindVertexArray(point_vao);

	glUniformMatrix4fv(glGetUniformLocation(point_shader, "view"), 1, GL_FALSE, glm::value_ptr(view));
	glDrawArrays(GL_POINTS, 0, world.numParticles());
}

GLuint Renderer::getPointVBO() const {
	return point_vbo;
}