#include "util.h"
#include "debug.h"
#include "defs.h"

#define STB_IMAGE_IMPLEMENTATION

#include <stb/stb_image.h>

#include <fstream>

static std::string readFile(const std::string &path) {
	std::ifstream fs(path);
	if (!fs.good()) {
		std::cerr << "Error opening file " << path << std::endl;
		std::exit(EXIT_FAILURE);
	}

	fs.seekg(0, std::ios::end);
	auto sz = fs.tellg();
	fs.seekg(0, std::ios::beg);

	std::string str(sz, 0);
	fs.read(&str[0], sz);
	return str;
}

static GLuint compileShader(const char *source, GLuint type) {
	auto id = glCreateShader(type);
	glShaderSource(id, 1, &source, nullptr);
	glCompileShader(id);
	CHECK_GL_SHADER_ERROR(id);
	return id;
}

GLuint glu::loadShaders(const std::string &vert_path, const std::string &frag_path) {
	std::string vs_str = readFile(vert_path);
	std::string fs_str = readFile(frag_path);

	// Compile shaders
	auto vs = compileShader(vs_str.c_str(), GL_VERTEX_SHADER);
	auto fs = compileShader(fs_str.c_str(), GL_FRAGMENT_SHADER);

	// Link program
	auto program = glCreateProgram();
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	CHECK_GL_PROGRAM_ERROR(program);

	// Cleanup
	glDeleteShader(vs);
	glDeleteShader(fs);

	return program;
}

GLuint loadTexture(const std::string &tex_path) {
	int width, height, bpp;
	stbi_set_flip_vertically_on_load(true);
	u8 *data = stbi_load(tex_path.c_str(), &width, &height, &bpp, 0);
	if (!data) {
		std::cerr << "Error loading texture " << tex_path << "!" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	auto type = (bpp == 3) ? GL_RGB : GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, type, width, height, 0, type, GL_UNSIGNED_BYTE, data);
	stbi_image_free(data);

	glGenerateMipmap(GL_TEXTURE_2D);
	return tex;
}