#pragma once

#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <cstdlib>
#include <iostream>
#include <string>

#define CHECK_GL_SUCCESS(x) \
    do { \
        if (!(x)) { \
            glfwTerminate(); \
            std::exit(EXIT_FAILURE); \
        } \
    } while (0)

#define CHECK_GL_SHADER_ERROR(id) \
	do { \
		GLint status = 0; \
		GLint length = 0; \
		glGetShaderiv(id, GL_COMPILE_STATUS, &status); \
		glGetShaderiv(id, GL_INFO_LOG_LENGTH, &length); \
		if (status != GL_TRUE) { \
			std::string log(length, 0); \
			glGetShaderInfoLog(id, length, nullptr, &log[0]); \
			std::cerr << "Function: " << __func__ \
					  << " Line: " << __LINE__ \
					  << " File: " << __FILE__ \
					  << " Status: " << status \
					  << " OpenGL Shader Error: Log = " << std::endl \
					  << &log[0] << std::endl; \
			glfwTerminate(); \
			std::exit(EXIT_FAILURE); \
		} \
	} while (0)

#define CHECK_GL_PROGRAM_ERROR(id) \
	do { \
		GLint status = 0; \
		GLint length = 0; \
		glGetProgramiv(id, GL_LINK_STATUS, &status); \
		glGetProgramiv(id, GL_INFO_LOG_LENGTH, &length); \
		if (status != GL_TRUE) { \
			std::string log(length, 0); \
			glGetProgramInfoLog(id, length, nullptr, &log[0]); \
			std::cerr << "Function: " << __func__ \
					  << " Line: " << __LINE__ \
					  << " File: " << __FILE__ \
					  << " OpenGL Program Error: Log = " << std::endl \
					  << &log[0] << std::endl; \
			glfwTerminate(); \
			std::exit(EXIT_FAILURE); \
		} \
	} while (0)

#define CHECK_GL_ERROR(stmt) \
	do { \
		{ stmt; } \
		GLenum error = GL_NO_ERROR; \
		if ((error = glGetError()) != GL_NO_ERROR) { \
			std::cerr << "Function: " << __func__ \
					  << " Line: " << __LINE__ \
					  << " File: " << __FILE__ \
					  << " OpenGL Error: code = " << error \
					  << " description = " << debugErrorToString(int(error)) \
					  << std::endl; \
			glfwTerminate(); \
			exit(EXIT_FAILURE); \
		} \
	} while (0)

// Too lazy to make a new file, so here's some CUDA helpers too!

# define CHECK_CUDA_ERROR(stmt) \
	do { \
		auto _chkcudaerror_result = (stmt); \
    	if (_chkcudaerror_result != cudaSuccess) { \
    		std::cerr << "Function: " << __func__ \
    				  << " Line: " << __LINE__ \
    				  << " File: " << __FILE__ \
    				  << " CUDA Error: code = " << _chkcudaerror_result \
    				  << std::endl; \
    		exit(EXIT_FAILURE); \
    	} \
	} while (0)

#define CHECK_KERNEL_ERROR(k) \
	do { \
		{ k; } \
		CHECK_CUDA_ERROR(cudaGetLastError()); \
	} while (0)

const char *debugErrorToString(int error);