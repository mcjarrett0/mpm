#include "grid.h"
#include "debug.h"
#include "util.h"

__device__
static Eigen::Vector3f grid2pos(u32 pos, const int &gridWidth) {
	auto x = pos % gridWidth;
	pos /= gridWidth;
	auto y = pos % gridWidth;
	pos /= gridWidth;
	return {x, y, pos};
}

Grid::Grid(float cellSize, u32 width) : _cellSize(cellSize), _width(width) {
	CHECK_CUDA_ERROR(cudaMalloc(&_devicePtr, size() * sizeof(Cell)));
}

void Grid::reset() const {
	auto width = _width;
	gpu::foreach(_devicePtr, size(), [=] __device__ (u32 i, Cell &c) {
		c.pos = grid2pos(i, width);
		c.vel.setZero();
		c.mass = 0;
	});
}

Cell * Grid::devicePtr() const {
	return _devicePtr;
}

float Grid::cellSize() const {
	return _cellSize;
}

u32 Grid::width() const {
	return _width;
}

u32 Grid::size() const {
	return _width * _width * _width;
}