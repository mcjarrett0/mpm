#pragma once

#include "config.h"
#include "defs.h"
#include "world.h"

#include <Eigen/Dense>
#include <glad/gl.h>
#include <glm/matrix.hpp>

class Renderer {
	const glm::mat4 proj;
	glm::mat4 view;
	const GLuint textures[1], floor_shader, point_shader;
	const World &world;

	GLuint floor_vbo, floor_vao, floor_ebo, point_vbo, point_vao, point_ebo;

	void drawFloor();
	void drawPoints();

public:
	Renderer(const Configuration &cfg, u32 width, u32 height, const World &world);

	void render();

	GLuint getPointVBO() const;
};