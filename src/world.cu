#include "world.h"
#include "debug.h"

#include <svd/svd3_cuda.h>

#include <iostream>
#include <cstdlib>

__device__
static void svd(const Eigen::Matrix3f &m, Eigen::Matrix3f &u, Eigen::Matrix3f &s, Eigen::Matrix3f &v) {
	svd3::svd(m(0), m(3), m(6), m(1), m(4), m(7), m(2), m(5), m(8),
			  u(0), u(3), u(6), u(1), u(4), u(7), u(2), u(5), u(8),
			  s(0), s(4), s(8),
			  v(0), v(3), v(6), v(1), v(4), v(7), v(2), v(5), v(8));
}

__device__
static float det(const Eigen::Matrix3f &m) {
	return m(0) * (m(4) * m(8) - m(7) * m(5)) -
		   m(3) * (m(1) * m(8) - m(7) * m(2)) +
		   m(6) * (m(1) * m(5) - m(4) * m(2));
}

__device__
Eigen::Matrix3f Particle::stress() const {
	Eigen::Matrix3f u, _, v;
	svd(F, u, _, v);

	auto J = det(F);
	auto P = 2.0f * mat.mu * (F - u * v.transpose()) * F.transpose() +
			 (mat.lambda * J * (J - 1)) * Eigen::Matrix3f::Identity();
	return volume * P;
}

void World::addModel(const Model &model, const Eigen::Vector3f &pos, const Eigen::Vector3f &vel, const Material &mat) {
	for (auto &v : model.getVertices())
		particles.emplace_back(Particle(pos + v, vel, mat));

	// Adding a model invalidates the old device memory, rebuild it
	auto sz = numParticles() * sizeof(Particle);
	CHECK_CUDA_ERROR(cudaFree(_devicePtr));
	CHECK_CUDA_ERROR(cudaMalloc(&_devicePtr, sz));
	CHECK_CUDA_ERROR(cudaMemcpy(
			(void *) _devicePtr,
			(void *) particles.data(),
			sz,
			cudaMemcpyHostToDevice));
}

Particle *World::devicePtr() const {
	return _devicePtr;
}

u32 World::numParticles() const {
	return particles.size();
}