#pragma once

#include "defs.h"

#include <cstdint>
#include <string>

// TODO accept program arguments
struct Configuration {
	// Sim params
	u32 gridLength = 100;
	float cellSize = 0.1f;
	float timeStep = 0.003f;
	float gravity = -9.81f;

	// Renderer params
	float pointSize = 40.0f;
	float fov = 75.0f;

	std::string model_name = "res/monkey.obj";
};