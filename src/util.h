#pragma once

#include <glad/gl.h>

#include <string>

// Unfortunately overloaded name, but the meaning is correct
namespace glu
{

GLuint loadTexture(const std::string &tex_path);

GLuint loadShaders(const std::string &vert_path, const std::string &frag_path);

}

namespace gpu
{
	template<class F>
	__global__
	void kernel(F f) {
		f(threadIdx.x + 32 * blockIdx.x);
	}

	template <typename T, class F>
	void foreach(T *vec, unsigned int size, F f) {
		auto warps = (size + 31) / 32;
		kernel<<<warps, 32>>>([=] __device__ (unsigned int tid) {
			if (tid < size)
				f(tid, vec[tid]);
		});
		cudaDeviceSynchronize();
	}
}