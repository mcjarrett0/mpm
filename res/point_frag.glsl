#version 460 core

out vec4 out_Color;

flat in float w;

void main() {
    vec2 circCoord = 2.0 * gl_PointCoord - 1.0;
    if (dot(circCoord, circCoord) > 1.0) {
        discard;
    }
    out_Color = vec4(0.8 + 0.4 * max(1 - w/10, 0));
}
