#version 460 core

layout (location = 0) in vec4 pos;
layout (location = 1) in vec2 in_tex_coord;

out vec2 tex_coord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main() {
    gl_Position = proj * view * model * pos;
    tex_coord = in_tex_coord;
}

