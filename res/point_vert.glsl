#version 460 core

layout (location = 0) in vec4 pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;
uniform float size;

flat out float w;

void main() {
    gl_Position = proj * view * model * pos;
    gl_PointSize = size / gl_Position.w;
    w = gl_Position.w;
}