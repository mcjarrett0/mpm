#version 460 core

in vec2 tex_coord;

uniform sampler2D tex1;

out vec4 out_Color;

void main() {
    if (mod(mod(floor(tex_coord.x / 4), 10) + mod(floor(tex_coord.y / 4), 10), 2) == 0)
        out_Color = vec4(0.8, 0.8, 0.8, 0.0);
    else
        out_Color = vec4(0.2);
}