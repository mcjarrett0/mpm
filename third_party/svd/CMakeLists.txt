cmake_minimum_required(VERSION 3.10)
project(svd)

add_library(svd INTERFACE)
target_include_directories(svd INTERFACE include)